Introduction
Role class module is used to add user's roles as class in page <body>.

Installation
Role class can be installed easily.
Download the module to your DRUPAL_ROOT/modules/contribe/ directory, or where ever you install contrib modules on your site.
Go to Admin > Extend and enable the module.
